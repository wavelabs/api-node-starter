# NodeJS Starter App

## Installation
1. Install nodeJS from [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
2. Clone this project or download the `.zip` file
3. Go to the project directory from your terminal
4. Run `npm install`
5. Run `node app.js`
6. Go to browser and hit `http://localhost:3000/book`